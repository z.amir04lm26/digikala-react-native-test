import 'react-native-gesture-handler';
import React from 'react';
import {I18nManager, StatusBar} from 'react-native';
import {Provider} from 'react-redux';

import {store} from './src/redux/store';
import TabNavigator from './src/components/common/TabNavigator';
import colors from './src/utilities/colors';
import {Root} from 'native-base';

I18nManager.allowRTL(false);

const App = () => {
  return (
    <Provider store={store}>
      <StatusBar
        backgroundColor={colors.primary.darker}
        barStyle="light-content"
      />
      <Root>
        <TabNavigator />
      </Root>
    </Provider>
  );
};

export default App;
