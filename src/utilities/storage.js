import AsyncStorage from '@react-native-async-storage/async-storage';

export const storeData = (key, value) =>
  AsyncStorage.setItem('@' + key, JSON.stringify(value));

export const getData = async (key) => {
  const jsonValue = await AsyncStorage.getItem('@' + key);
  if (jsonValue != null) return JSON.parse(jsonValue);
  throw Error('noData');
};
