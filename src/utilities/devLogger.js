const logger = (color, ...args) => {
  return void console.log(`${color ? color : '\x1b[36m\x1b[34m'}${args.length > 1 ? args.join('\r\n') : args}\x1b[0m`);
};

const devLogger = (title, type = 'ERROR', ...args) => {
  if (__DEV__) {
    logger(null, title);
    switch (type) {
      case 'ERROR':
        return void logger('\x1b[31m', args);
      case 'SUCCESS':
        return void logger('\x1b[32m', args);
      default:
        return void logger('\x1b[34m', args);
    }
  }
};

export default devLogger;
