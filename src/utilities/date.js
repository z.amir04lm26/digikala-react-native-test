export const formatDate = (date) => {
  try {
    const _date = new Date(date);
    return _date.toDateString().split(' ').splice(1).join(' ');
  } catch (error) {
    return date;
  }
};
