import {Toast} from 'native-base';

export default function showToast(message, type) {
  Toast.show({
    text: message,
    duration: 3000,
    position: 'bottom',
    type: type,
    textStyle: {color: '#fff', fontFamily: 'Rubik Italic', fontSize: 16},
  });
}
