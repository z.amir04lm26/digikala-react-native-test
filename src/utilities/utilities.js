export const checkResult = (data) => Array.isArray((data || {}).results);

export const fixDirectorName = (directorName) =>
  String(directorName).split(' ').splice(1).join(' ');

export const isEmpty = (arr) => !(Array.isArray(arr) && arr.length > 0);
