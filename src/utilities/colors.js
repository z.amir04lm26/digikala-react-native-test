export default {
  white: {
    base: '#fff',
  },
  black: {
    base: '#000',
  },
  primary: {
    darker: '#3b0622',
    dark: '#880e4f',
    base: '#c2185b',
    light: '#ff4081',
    lighter: '#ff80ab',
    whitish: '#fce4ec',
  },
  seconday: {
    base: '#797ae9',
  },
};
