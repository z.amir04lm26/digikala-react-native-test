import {Container, Text} from 'native-base';
import React from 'react';

import FormButton from './common/FormButton';
import commonStyles from '../styles/common';
import {clearAuth} from '../redux/actions/auth';
import {useDispatch} from 'react-redux';

const Greeting = ({name}) => {
  const dispatch = useDispatch();

  const handleLogout = () => {
    dispatch(clearAuth());
  };

  return (
    <Container style={commonStyles.center}>
      <Text style={commonStyles.title}>Welcome {name}</Text>
      <FormButton text="Logout" clicked={handleLogout} />
    </Container>
  );
};

export default Greeting;
