import React, {useState} from 'react';
import {
  Container,
  Header,
  Text,
  Content,
  Form,
  Item,
  Input,
  Icon,
  Button,
} from 'native-base';
import {useDispatch} from 'react-redux';

import styles from '../styles/login';
import FormInput from './common/FormInput';
import FormButton from './common/FormButton';
import {loginUser} from '../services/authService';
import showToast from '../utilities/toast';
import {clearAuth, setAuth} from '../redux/actions/auth';

const Login = () => {
  const [userName, setUserName] = useState('hriks');
  const [password, setPassword] = useState('gt4043@1');

  const dispatch = useDispatch();

  const handleLogin = () => {
    loginUser({
      username: userName,
      password,
    })
      .then(({data}) => {
        const token = data.token;
        if (token) {
          dispatch(setAuth(token, userName));
          showToast('You have successfuly logged in.', 'success');
        } else {
          showToast('Something went wrong!', 'error');
        }
      })
      .catch((err) => dispatch(clearAuth()));
  };

  return (
    <Container>
      <Content contentContainerStyle={styles.content}>
        <Form style={styles.form}>
          <FormInput
            placeholder="Username"
            icon="user"
            value={userName}
            textChanged={(text) => setUserName(text)}
          />
          <FormInput
            placeholder="Password"
            icon="key"
            value={password}
            textChanged={(text) => setPassword(text)}
            isSecure
          />
          <FormButton text="Login" clicked={handleLogin} />
        </Form>
      </Content>
    </Container>
  );
};

export default Login;
