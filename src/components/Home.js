import {Container} from 'native-base';
import React, {useEffect, useState} from 'react';

import MoviesList from './common/MoviesList';
import SearchBox from './common/SearchBox';
import Loading from './common/Loading';
import Refresh from './common/Refresh';
import NoContent from './common/NoContent';
import {searchMovie} from '../services/movieServices';
import {checkResult, isEmpty} from '../utilities/utilities';

const Home = () => {
  const [movies, setMovies] = useState([]);
  const [isSearching, setIsSearching] = useState(false);

  let timer;

  useEffect(() => {
    fetchMovies();
    return () => {
      clearTimeout(timer);
    };
  }, []);

  async function fetchMovies(movieName) {
    setIsSearching(true);
    try {
      const {data} = await searchMovie(movieName);
      if (checkResult(data)) {
        setMovies(data.results);
        setIsSearching(false);
      } else setFetchInterval();
    } catch (error) {
      setFetchInterval();
    }
  }

  const setFetchInterval = () => {
    clearTimeout(timer);
    timer = setTimeout(fetchMovies, 20000);
  };

  const handleSearch = (searchInput) => {
    if (String(searchInput).trim() !== '') fetchMovies(searchInput);
  };

  const hasNoMovie = isEmpty(movies);

  return (
    <Container>
      <SearchBox searchHandler={handleSearch} />
      <Refresh
        isRefreshing={isSearching}
        isCenter={isSearching || hasNoMovie}
        onRefresh={fetchMovies}>
        {!isSearching ? (
          <>{!hasNoMovie ? <MoviesList movies={movies} /> : <NoContent />}</>
        ) : (
          <Loading />
        )}
      </Refresh>
    </Container>
  );
};

export default Home;
