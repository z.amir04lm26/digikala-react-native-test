import {Button, Text} from 'native-base';
import React, {memo, useEffect} from 'react';
import {RefreshControl, SafeAreaView, ScrollView} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {useDispatch, useSelector} from 'react-redux';

import styles from '../styles/catergoryList';
import colors from '../utilities/colors';
import {initCategories} from '../redux/actions/categories';
import Loading from './common/Loading';
import {isEmpty} from '../utilities/utilities';

const CatergoryList = ({navigation}) => {
  const categories = useSelector((state) => state.categories);
  const dispatch = useDispatch();

  let timer;

  useEffect(() => {
    setFetchTimout();
    return () => {
      clearTimeout(timer);
    };
  }, []);

  async function fetchCategories() {
    if (isEmpty(categories.list)) {
      fetchRequest();
      setFetchTimout();
    }
  }

  const fetchRequest = () => void dispatch(initCategories());

  const setFetchTimout = () => {
    clearTimeout(timer);
    timer = setTimeout(fetchCategories, 20000);
  };

  const navigateToMovies = (catergoryName) => {
    navigation.navigate('Movies', {
      category: catergoryName,
    });
  };

  return (
    <>
      {(categories || {}).isLoading === false ? (
        <SafeAreaView style={styles.container}>
          <ScrollView
            contentContainerStyle={styles.scrollView}
            refreshControl={
              <RefreshControl
                refreshing={(categories || {}).isLoading}
                onRefresh={fetchRequest}
              />
            }>
            {categories.list.map((catergory) => (
              <LinearGradient
                key={catergory.id}
                style={styles.categoryContainer}
                start={{x: 0.7, y: 0}}
                colors={[colors.seconday.base, colors.primary.base]}>
                <Button
                  style={styles.categoryItem}
                  onPress={() => {
                    navigateToMovies(catergory.name);
                  }}>
                  <Text style={styles.categoryText}>{catergory.name}</Text>
                </Button>
              </LinearGradient>
            ))}
          </ScrollView>
        </SafeAreaView>
      ) : (
        <Loading />
      )}
    </>
  );
};

export default memo(CatergoryList);
