import {Header} from 'native-base';
import React from 'react';
import {StyleSheet} from 'react-native';

import colors from '../../utilities/colors';

const Head = ({children}) => {
  return (
    <Header style={styles.header} androidStatusBarColor={colors.primary.darker}>
      {children}
    </Header>
  );
};

export default Head;

const styles = StyleSheet.create({
  header: {
    backgroundColor: colors.white.base,
    height: 60,
    marginBottom: 15,
    paddingLeft: 20,
    paddingRight: 20,
  },
});
