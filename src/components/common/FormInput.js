import React from 'react';
import {StyleSheet} from 'react-native';
import {Item, Input, Icon} from 'native-base';

const FormInput = ({placeholder, icon, value, textChanged, isSecure}) => {
  return (
    <Item rounded style={styles.item}>
      <Icon active name={icon} type="FontAwesome" />
      <Input
        placeholder={placeholder}
        secureTextEntry={isSecure}
        value={value}
        onChangeText={textChanged}
      />
    </Item>
  );
};

export default FormInput;

const styles = StyleSheet.create({
  item: {
    width: '80%',
    alignSelf: 'center',
    marginTop: '4%',
    marginBottom: '1%',
    paddingLeft: 10,
    paddingRight: 10,
  },
});
