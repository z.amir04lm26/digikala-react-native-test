import {Badge, Text} from 'native-base';
import React from 'react';
import colors from '../../utilities/colors';

const CardBadge = ({text}) => {
  return (
    <Badge style={{margin: 5, backgroundColor: colors.primary.base}}>
      <Text style={{fontSize: 12}}>{text}</Text>
    </Badge>
  );
};

export default CardBadge;
