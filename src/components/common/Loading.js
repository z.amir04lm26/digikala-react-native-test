import React from 'react';
import {View} from 'react-native';
import {Content} from 'native-base';
var Spinner = require('react-native-spinkit');

import commonStyles from '../../styles/common';
import colors from '../../utilities/colors';

const Loading = () => {
  return (
    <Content contentContainerStyle={commonStyles.center}>
      <View>
        <Spinner
          size={100}
          type="Wave"
          color={colors.primary.dark}
          style={{marginTop: -60}}
        />
      </View>
    </Content>
  );
};

export default Loading;
