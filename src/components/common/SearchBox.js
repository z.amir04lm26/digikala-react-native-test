import React, {useState} from 'react';
import {StyleSheet} from 'react-native';
import {Icon, Button, Body, Item, Input} from 'native-base';

import Header from './Header';
import colors from '../../utilities/colors';

const SearchBox = ({searchHandler}) => {
  const [searchInput, setSearchInput] = useState();

  const handleSearch = () => {
    searchHandler(searchInput);
    setSearchInput('');
  };

  return (
    <Header>
      <Body>
        <Item rounded style={styles.item}>
          <Button transparent onPressIn={handleSearch}>
            <Icon type="FontAwesome" name="search" style={styles.icon} />
          </Button>
          <Input
            style={styles.input}
            placeholder="Iron Man"
            value={searchInput}
            onChangeText={(value) => setSearchInput(value)}
          />
        </Item>
      </Body>
    </Header>
  );
};

export default SearchBox;

const styles = StyleSheet.create({
  item: {
    paddingLeft: 10,
    paddingRight: 10,
    height: '80%',
  },
  icon: {
    color: colors.seconday.base,
  },
  input: {
    color: colors.primary.darker,
  },
});
