import React from 'react';
import {StyleSheet} from 'react-native';
import {Button, Text} from 'native-base';
import colors from '../../utilities/colors';
import commonStyles from '../../styles/common';

const FormButton = ({text, clicked}) => {
  return (
    <Button style={styles.button} rounded block onPressIn={clicked}>
      <Text style={commonStyles.text}>{text}</Text>
    </Button>
  );
};

export default FormButton;

const styles = StyleSheet.create({
  button: {
    width: '80%',
    alignSelf: 'center',
    marginTop: '12%',
    marginBottom: '1%',
    backgroundColor: colors.primary.dark,
  },
});
