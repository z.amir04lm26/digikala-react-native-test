import React from 'react';
import {Content, Text} from 'native-base';

import commonStyles from '../../styles/common';

const NoContent = () => {
  return (
    <Content contentContainerStyle={commonStyles.center}>
      <Text style={{marginTop: -60}}>Nothing found!</Text>
    </Content>
  );
};

export default NoContent;
