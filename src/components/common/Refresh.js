import React from 'react';
import {RefreshControl} from 'react-native';
import {Content} from 'native-base';

import commonStyles from '../../styles/common';

const Refresh = ({children, isRefreshing, isCenter, onRefresh}) => {
  return (
    <Content
      contentContainerStyle={isCenter ? commonStyles.center : null}
      refreshControl={
        <RefreshControl refreshing={isRefreshing} onRefresh={onRefresh} />
      }>
      {children}
    </Content>
  );
};

export default Refresh;
