import React, {memo} from 'react';
import {Image} from 'react-native';
import {Card, CardItem, Text, Icon, Left, Body, View} from 'native-base';

import CardBadge from './Badge';
import {formatDate} from '../../utilities/date';
import commonStyles from '../../styles/common';
import styles from '../../styles/common/MovieCard';

const MovieCard = ({title, date, director, tags, rating}) => {
  return (
    <Card style={styles.card}>
      <CardItem>
        <Left>
          <Body>
            <Text>{title}</Text>
            <Text note>{formatDate(date)}</Text>
          </Body>
        </Left>
      </CardItem>
      <CardItem>
        <Body style={commonStyles.center}>
          <Image
            source={require('../../../assets/images/Film-icon.png')}
            style={styles.image}
          />
        </Body>
      </CardItem>
      <CardItem>
        <Body>
          <Text>
            Director: <Text note>{director}</Text>
          </Text>
          <Text>Tags:</Text>
          <View
            style={[
              commonStyles.center,
              commonStyles.flexRow,
              commonStyles.flexWrap,
            ]}>
            {Array.isArray(tags) &&
              tags.map((tag, index) => <CardBadge key={index} text={tag} />)}
          </View>
          <View style={[commonStyles.center, commonStyles.flexRow]}>
            <Icon type="FontAwesome" name="star" style={styles.ratingIcon} />
            <Text style={styles.ratingText}>{rating}</Text>
          </View>
        </Body>
      </CardItem>
    </Card>
  );
};

export default memo(MovieCard);
