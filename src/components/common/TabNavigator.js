import React from 'react';
import {Dimensions} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {Icon} from 'native-base';

import LoginScreen from '../../screens/login';
import HomeScreen from '../../screens/home';
import MoviesScreen from '../../screens/movies';
import CategoryListScreen from '../../screens/categoryList';
import colors from '../../utilities/colors';

const Tab = createMaterialTopTabNavigator();

const TabNavigator = () => {
  const {primary} = colors;

  return (
    <NavigationContainer independent={true}>
      <Tab.Navigator
        initialRouteName="Home"
        tabBarPosition="bottom"
        backBehavior="history"
        lazy={true}
        lazyPreloadDistance={2}
        removeClippedSubviews={true}
        initialLayout={{
          width: Dimensions.get('window').width,
        }}
        tabBarOptions={{
          labelStyle: {fontSize: 12},
          showIcon: true,
          pressColor: primary.base,
          indicatorStyle: {
            backgroundColor: primary.light,
          },
          activeTintColor: primary.lighter,
          inactiveTintColor: primary.whitish,
          style: {backgroundColor: primary.dark},
          allowFontScaling: true,
        }}>
        <Tab.Screen
          name="Login"
          component={LoginScreen}
          options={tabOptions('user')}
        />
        <Tab.Screen
          name="Home"
          component={HomeScreen}
          options={tabOptions('home')}
        />
        <Tab.Screen
          name="Movies"
          component={MoviesScreen}
          options={tabOptions('film')}
        />
        <Tab.Screen
          name="Categories"
          component={CategoryListScreen}
          options={tabOptions('list-alt')}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
};

export default TabNavigator;

const tabOptions = (name) => {
  return {
    tabBarIcon: ({color}) => (
      <Icon
        name={name}
        type="FontAwesome"
        style={{fontSize: 24, textAlign: 'center', color: color}}
      />
    ),
  };
};
