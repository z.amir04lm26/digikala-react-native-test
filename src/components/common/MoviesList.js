import React from 'react';
import {Content} from 'native-base';

import MovieCard from './MovieCard';
import commonStyles from '../../styles/common';
import {fixDirectorName} from '../../utilities/utilities';

const MoviesList = ({movies}) => {
  return (
    <Content contentContainerStyle={commonStyles.alignCenter}>
      {movies.map((movie, index) => (
        <MovieCard
          key={index}
          title={movie.title}
          date={movie.date_of_release}
          director={fixDirectorName(movie.director)}
          tags={movie.tags}
          rating={movie.rating}
        />
      ))}
    </Content>
  );
};

export default MoviesList;
