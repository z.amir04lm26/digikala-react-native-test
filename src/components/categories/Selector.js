import React, {memo} from 'react';
import {StyleSheet} from 'react-native';
import {Body, Form, Picker} from 'native-base';

import Header from '../common/Header';

const Selector = ({items, selectorHandler, selectedCategory}) => {
  let selectedIndex = 1;

  (() => {
    (items || []).map((ctg, index) => {
      if (ctg.name === selectedCategory) return void (selectedIndex = index);
    });
  })();

  return (
    <Header>
      <Body style={styles.body}>
        <Form>
          <Picker
            note
            mode="dropdown"
            style={styles.picker}
            selectedValue={selectedIndex}
            onValueChange={(value) => selectorHandler(value)}>
            {(items || []).map((item, index) => (
              <Picker.Item key={index} label={item.name} value={index} />
            ))}
          </Picker>
        </Form>
      </Body>
    </Header>
  );
};

export default memo(Selector);

const styles = StyleSheet.create({
  body: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  picker: {
    width: 160,
  },
});
