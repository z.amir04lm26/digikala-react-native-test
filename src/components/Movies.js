import React, {useEffect, useState} from 'react';
import {Container} from 'native-base';
import {useSelector} from 'react-redux';

import Selector from './categories/Selector';
import MoviesList from './common/MoviesList';
import Loading from './common/Loading';
import NoContent from './common/NoContent';
import {getMovies} from '../services/movieServices';
import {checkResult, isEmpty} from '../utilities/utilities';
import Refresh from './common/Refresh';

const Movies = ({params}) => {
  const [category, setCategory] = useState('action');
  const [movies, setMovies] = useState([]);
  const [isSearching, setIsSearching] = useState(false);

  const categories = useSelector((state) => state.categories);

  let timer;

  useEffect(() => {
    fetchMovies();
    return () => {
      clearTimeout(timer);
    };
  }, [category]);

  useEffect(() => {
    setCategory((params || {}).category);
  }, [params]);

  async function fetchMovies() {
    setIsSearching(true);
    try {
      const {data} = await getMovies(category);
      if (checkResult(data)) {
        setMovies(data.results);
        setIsSearching(false);
      } else setFetchInterval();
    } catch (error) {
      setFetchInterval();
    }
  }

  const setFetchInterval = () => {
    clearTimeout(timer);
    timer = setTimeout(fetchMovies, 20000);
  };

  const handleSelectorChanged = (index) => {
    setCategory((categories.list[index] || {}).name);
  };

  const hasNoMovie = isEmpty(movies);

  return (
    <Container>
      <Selector
        items={categories.list}
        selectorHandler={handleSelectorChanged}
        selectedCategory={category}
      />
      <Refresh
        isRefreshing={isSearching}
        isCenter={isSearching || hasNoMovie}
        onRefresh={fetchMovies}>
        {!isSearching ? (
          <>{!hasNoMovie ? <MoviesList movies={movies} /> : <NoContent />}</>
        ) : (
          <Loading />
        )}
      </Refresh>
    </Container>
  );
};

export default Movies;
