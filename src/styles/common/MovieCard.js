import {StyleSheet} from 'react-native';
import colors from '../../utilities/colors';

module.exports = StyleSheet.create({
  card: {
    width: '90%',
    marginBottom: 20,
  },
  image: {
    height: 200,
    width: 200,
  },
  badge: {
    margin: 10,
  },
  ratingIcon: {
    fontSize: 20,
    color: colors.seconday.base,
  },
  ratingText: {
    fontSize: 14,
    marginLeft: 10,
  },
});
