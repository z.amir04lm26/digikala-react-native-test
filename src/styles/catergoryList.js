import {StyleSheet} from 'react-native';

import colors from '../utilities/colors';

module.exports = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 40,
    paddingBottom: 20,
    backgroundColor: colors.white.base,
  },
  scrollView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignItems: 'center',
  },
  categoryContainer: {
    width: 150,
    height: 150,
    margin: 20,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
  },
  categoryItem: {
    backgroundColor: 'white',
    opacity: 0.75,
    alignSelf: 'center',
    borderRadius: 2,
  },
  categoryText: {
    justifyContent: 'center',
    textAlign: 'center',
    fontFamily: 'Rubik Italic',
    fontSize: 18,
    fontWeight: 'bold',
    color: colors.primary.darker,
  },
});
