import {StyleSheet} from 'react-native';

const commonStyles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  flexRow: {
    flexDirection: 'row',
  },
  flexWrap: {
    flexWrap: 'wrap',
  },
  alignCenter: {
    alignItems: 'center',
  },
  text: {
    fontFamily: 'Rubik Regular',
    fontSize: 16,
  },
  textItalic: {
    fontFamily: 'Rubik Italic',
    fontSize: 16,
  },
  title: {
    fontFamily: 'Rubik Italic',
    fontSize: 20,
    fontWeight: 'bold',
  },
});

export default commonStyles;
