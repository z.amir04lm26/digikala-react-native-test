import axios from 'axios';

import devLogger from '../utilities/devLogger';
import showToast from '../utilities/toast';

axios.defaults.headers.post['Content-Type'] = 'application/json';

// const token = localStorage.getItem("token");

// if (token) axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;

axios.interceptors.response.use(null, (error) => {
  const res = (error || {}).response;
  const expectedError = res && res.status >= 400 && res < 500;
  if (!expectedError) {
    devLogger('Axios Error:', 'ERROR', error);
    showToast('An error has been occurred.', 'danger');
  }
  if (res.status == 400) showToast('Wrong credentials!', 'danger');
  return Promise.reject(res);
});

module.exports = {
  get: axios.get,
  post: axios.post,
  delete: axios.delete,
  put: axios.put,
};
