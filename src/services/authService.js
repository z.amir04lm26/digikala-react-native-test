import {post} from './httpServices';
import config from './config.json';

export const loginUser = (user) => {
  return post(`${config.BASE_URL}/user/auth-token`, JSON.stringify(user));
};
