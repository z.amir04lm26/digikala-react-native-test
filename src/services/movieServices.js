import {get} from './httpServices';
import config from './config.json';

export const getCategories = () => {
  return get(`${config.BASE_URL}/category`);
};

export const getMovies = (tag, limit = 20, offset = 0) => {
  return get(
    `${config.BASE_URL}/movie/?limit=${limit}&offset=${offset}&tags=${tag}`,
  );
};

export const searchMovie = (keyword) => {
  return get(`${config.BASE_URL}/movie${keyword ? '/?search=' + keyword : ''}`);
};
