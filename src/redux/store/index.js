import {createStore, applyMiddleware} from 'redux';
import {reducers} from './../reducers';
import thunk from 'redux-thunk';
import {initAuth} from '../actions/auth';
import {initCategories} from '../actions/categories';

export const store = createStore(reducers, applyMiddleware(thunk));

store.dispatch(initAuth());
store.dispatch(initCategories());

//subscribe
// store.subscribe(() => console.log(store.getState()));
