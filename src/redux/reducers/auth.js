export const authReducer = (state = {}, action) => {
  const payload = action.payload;
  switch (action.type) {
    case 'SET_TOKEN':
      return {
        ...state,
        token: payload,
      };
    case 'CLEAR_TOKEN':
      return {
        ...state,
        token: payload,
      };
    case 'SET_NAME':
      return {
        ...state,
        name: payload,
      };
    case 'CLEAR_NAME':
      return {
        ...state,
        name: payload,
      };
    default:
      return state;
  }
};
