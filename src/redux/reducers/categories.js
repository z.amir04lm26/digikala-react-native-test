export const categoriesReducer = (state = {isLoading: false}, action) => {
  const payload = action.payload;

  switch (action.type) {
    case 'SET_CATEGORIES':
      return {
        ...state,
        list: [...payload],
      };
    case 'SET_LOADING':
      return {
        ...state,
        isLoading: payload,
      };
    default:
      return state;
  }
};
