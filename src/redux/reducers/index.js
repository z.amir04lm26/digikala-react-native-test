import {combineReducers} from 'redux';
import {authReducer} from './auth';
import {categoriesReducer} from './categories';

export const reducers = combineReducers({
  auth: authReducer,
  categories: categoriesReducer,
});
