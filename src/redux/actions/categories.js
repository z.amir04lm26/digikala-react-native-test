import devLogger from '../../utilities/devLogger';
import {getCategories} from '../../services/movieServices';
import {checkResult} from '../../utilities/utilities';

export const initCategories = () => {
  return async (dispatch) => {
    try {
      await dispatch({type: 'SET_LOADING', payload: true});
      const {data} = await getCategories();
      if (checkResult(data)) {
        await dispatch({type: 'SET_CATEGORIES', payload: data.results});
        await dispatch({type: 'SET_LOADING', payload: false});
      }
    } catch (error) {
      devLogger('initCategories Error', 'ERROR', error);
    }
  };
};
