import devLogger from '../../utilities/devLogger';
import {getData, storeData} from '../../utilities/storage';

export const initAuth = () => {
  return async (dispatch) => {
    try {
      const userData = await getData('user');
      await dispatch({type: 'SET_TOKEN', payload: (userData || {}).token});
      await dispatch({type: 'SET_NAME', payload: (userData || {}).name});
    } catch (error) {
      devLogger('initAuth Storage Error', 'ERROR', error);
    }
  };
};

export const setAuth = (token, name) => {
  return async (dispatch) => {
    try {
      await storeData('user', {
        name,
        token,
      });
    } catch (error) {
      devLogger('setAuth Storage Error', 'ERROR', error);
    }
    await dispatch({type: 'SET_TOKEN', payload: token});
    await dispatch({type: 'SET_NAME', payload: name});
  };
};

export const clearAuth = () => {
  return async (dispatch) => {
    try {
      await storeData('user', null);
    } catch (error) {
      devLogger('clearAuth Storage Error', 'ERROR', error);
    }
    await dispatch({type: 'CLEAR_TOKEN', payload: null});
    await dispatch({type: 'CLEAR_NAME', payload: null});
  };
};
