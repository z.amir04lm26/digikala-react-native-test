import React from 'react';
import {useSelector} from 'react-redux';

import Login from '../components/Login';
import Greeting from '../components/Greeting';

const LoginScreen = () => {
  const auth = useSelector((state) => state.auth);

  return <>{!auth.token ? <Login /> : <Greeting name={auth.name} />}</>;
};

export default LoginScreen;
