import React from 'react';

import Movies from '../components/Movies';

const MoviesScreen = ({route}) => {
  return <Movies params={route.params} />;
};

export default MoviesScreen;
