import React from 'react';

import CategoryList from '../components/CategoryList';

const CategoryListScreen = ({navigation}) => {
  return <CategoryList navigation={navigation} />;
};

export default CategoryListScreen;
